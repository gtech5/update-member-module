package com.rezza.update.controller;

import lombok.Data;

import java.util.Date;

@Data
public class RequestModel {
    private String phone;
    private String email;
    private String firstName;
    private String lastName;
    private String gender;
    private Date dateOfBirth;
}
