package com.rezza.update.process;

import com.rezza.update.controller.RequestModel;
import com.rezza.update.db.mapper.TbMemberMapper;
import com.rezza.update.db.model.TbMember;
import com.rezza.update.db.model.TbMemberExample;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UpdateProcess {
    @Autowired
    private TbMemberMapper memberMapper;

    public int update(RequestModel requestModel) {
        TbMember member = new TbMember();
        member.setTbmFirstName(requestModel.getFirstName());
        member.setTbmLastName(requestModel.getLastName());
        member.setTbmGender(requestModel.getGender());
        member.setTbmDateOfBirth(requestModel.getDateOfBirth());

        TbMemberExample tbMemberExample = new TbMemberExample();
        tbMemberExample.createCriteria()
                .andTbmEmailEqualTo(requestModel.getEmail())
                .andTbmMobilePhoneEqualTo(requestModel.getPhone());
        return memberMapper.updateByExampleSelective(member , tbMemberExample);
    }
}